import Foundation
import Combine

var subscriptions = Set<AnyCancellable>()

example(of: "Publisher") {
  // 1
let myNotification = Notification.Name("MyNotification")
// 2
let publisher = NotificationCenter.default .publisher(for: myNotification, object: nil)

    let notificationCenter = NotificationCenter.default

    let observer = notificationCenter.addObserver(forName: myNotification, object: nil, queue: nil) { notification in
        print("Received notification: \(notification)")
    }

    notificationCenter.post(Notification(name: myNotification, object: nil, userInfo: nil))

    notificationCenter.removeObserver(observer)
}

example(of: "Subscriber") {
    let myNotification = Notification.Name("MyNotification")
    // 2
    let publisher = NotificationCenter.default .publisher(for: myNotification, object: nil)

        let notificationCenter = NotificationCenter.default
    let subscriber = publisher
        .sink { (notification) in
            print("Received notification: \(notification)")
        }
    notificationCenter.post(Notification(name: myNotification))
    subscriber.cancel()
}

example(of: "Just") {
    let just = Just("Hello World")

    let _ = just.sink {
        print("Receivd completion ", $0)
    } receiveValue: { value in
        print("Received value \(value)")
    }

    let _ = just.sink {
        print("Received completion for second", $0)
    } receiveValue: { value in
        print("Received value for second: \(value)")
    }
}

example(of: "assign(to: on:)") {
    class SomeObject {
        var value: String = "" {
            didSet {
                print("New Value: \(value)")
            }
        }
    }

    let object = SomeObject()

    let publisher = ["Hello", "Workd"].publisher

    let _ = publisher.assign(to: \.value, on: object)
}

example(of: "Custom Subscriber") {
    let publisher = (1...6).publisher

    class CustomSubscriber: Subscriber {
        typealias Input = Int
        typealias Failure = Never

        func receive(subscription: Subscription) {
            subscription.request(.max(3))
        }

        func receive(_ input: Int) -> Subscribers.Demand {
            print("Receoved subscription value ", input)
            return .none
        }

        func receive(completion: Subscribers.Completion<Never>) {
            print("Receoved subscription completion ", completion)
        }
    }

    let subscriber = CustomSubscriber()

    publisher.subscribe(subscriber)
}

example(of: "Future") {
    func futureIncrement(integer: Int, afterDelay delay: TimeInterval) -> Future<Int, Never> {
        return Future<Int, Never> { promise in
            DispatchQueue.global().asyncAfter(deadline: .now() + delay) {
                print("Original")
                promise(.success(integer + 1))
            }
        }
    }

//    let futureIncrementPublisher = futureIncrement(integer: 83, afterDelay: 5)
//
//    futureIncrementPublisher.sink {
//        print("Completion ", $0)
//    } receiveValue: {
//        print("Value ", $0)
//    }
//    .store(in: &subscriptions)
//
//    futureIncrementPublisher.sink {
//        print("Second Completion ", $0)
//    } receiveValue: {
//        print("Second Value ", $0)
//    }
//    .store(in: &subscriptions)
}


example(of: "PassthroughSubject") {
    enum MyError: Error {
        case test
    }

    class StringSubscriber: Subscriber {
        typealias Input = String
        typealias Failure = MyError

        func receive(subscription: Subscription) {
            subscription.request(.max(2))
        }

        func receive(_ input: String) -> Subscribers.Demand {
            print("Received value ", input)

            return input == "World" ? .max(2) : .none
        }

        func receive(completion: Subscribers.Completion<Failure>) {
            print("Received completion ", completion)
        }
    }

    let stringSubscriber = StringSubscriber()
    let subject = PassthroughSubject<String, MyError>()

    subject.subscribe(stringSubscriber)

    let subscription = subject
        .sink { print("Received completion in sink", $0)
        } receiveValue: { print("Received value in sink", $0) }
    subject.send("Hello")
    subject.send("World")

    subscription.cancel()
    subject.send("Still There?")

    subject.send(completion: .failure(MyError.test))
    subject.send(completion: .finished)
    subject.send("Another one")
}

example(of: "CurrentValueSubject") {
    let currentValueSubject = CurrentValueSubject<Int, Never>(0)

    currentValueSubject.sink(receiveValue: { print("Received value", $0) })

    currentValueSubject.send(1)

    print("Getting value ", currentValueSubject.value)
    currentValueSubject.value = 2

    currentValueSubject
        .print()
        .sink(receiveValue: { print("Second sink Received value", $0) })

    currentValueSubject.send(completion: .finished)
}

example(of: "Dynamcally adjusted demand") {
    final class IntSubscriber: Subscriber {
        typealias Input = Int
        typealias Failure = Never

        func receive(subscription: Subscription) {
            subscription.request(.max(2))
        }

        func receive(completion: Subscribers.Completion<Never>) {
            print("Received completion ", completion)
        }

        func receive(_ input: Int) -> Subscribers.Demand {
            print("Received value ", input)
            switch input {
            case 1:
                return .max(2)
            case 3:
                return .max(1)
            default:
                return .none
            }
        }
    }

    let subscriber = IntSubscriber()
    let publisher = PassthroughSubject<Int, Never>()

    publisher.subscribe(subscriber)

    publisher.send(1)
    publisher.send(2)
    publisher.send(3)
    publisher.send(4)
    publisher.send(5)
    publisher.send(6)
}

example(of: "Type erasure") {
    let subject = PassthroughSubject<Int, Never>()

    let publisher = subject.eraseToAnyPublisher()

    publisher
        .sink(receiveValue: { print("Received value", $0) })

    subject.send(1)
}

/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.
