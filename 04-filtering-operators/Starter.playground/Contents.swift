import Foundation
import Combine

var subscriptions = Set<AnyCancellable>()

example(of: "Filter") {
    (1 ... 10).publisher
        .filter{ $0 % 3 == 0 }
        .sink { print("Recieved value ", $0) }
}

example(of: "Remove Duplicates") {

    "a a a b h j k k w w l w a f"
        .components(separatedBy: " ")
        .publisher
        .removeDuplicates()
        .sink { print("Recieved value ", $0) }


}

example(of: "CompactMap") {
    ["asdas", "23", "3.13", "2,12", "3.12.4"]
        .publisher
        .compactMap { Float($0) }
        .sink { print("Recieved value ", $0) }
}

example(of: "IgnoreOutput") {
    (1...10000).publisher
        .ignoreOutput()
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of: "firstWhere") {
    let numbers = (1...10)
        .publisher

    numbers
        .print("numbers")
        .first{ $0 % 2 == 0 }
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of:"Drop first") {
    (1...10)
        .publisher
        .dropFirst(8)
        .sink { print("Recieved value ", $0) }
}

example(of:"DropWhile") {
    (1...20)
        .publisher
        .drop(while: {$0 % 4 == 0})
        .sink { print("Recieved value ", $0) }
}

example(of:"DropWhile vs CompactMap") {
    (1...20)
        .publisher
        .compactMap( {$0 % 4 == 0})
        .sink { print("Recieved value ", $0) }
}

example(of:"DropUntilOutpuFrom") {
    let dependencyStream = PassthroughSubject<Void, Never>()

    let dependentStream = PassthroughSubject<Int, Never>()

    dependentStream
        .drop(untilOutputFrom: dependencyStream)
        .sink { print("Recieved value ", $0) }

    (1 ... 5).forEach{
        dependentStream.send($0)

        if $0 == 3 { dependencyStream.send() }
    }
}

example(of:"Prefix first") {
    (1...10)
        .publisher
        .print("number")
        .prefix(3)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of:"PrefixWhile") {
    (1...20)
        .publisher
        .prefix(while: {$0 % 7 != 0})
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of: "Prefix until") {
    let thresholdStream = PassthroughSubject<Void, Never>()
    let dependentStream = PassthroughSubject<Int, Never>()

    dependentStream
        .prefix(untilOutputFrom: thresholdStream)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    (1 ... 5).forEach {
        dependentStream.send($0)
        if $0 == 3 { thresholdStream.send() }
    }
}

/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.
