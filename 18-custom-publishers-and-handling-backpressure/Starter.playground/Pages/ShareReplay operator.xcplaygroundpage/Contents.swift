import Foundation
import Combine

final class ShareReplaySubscription<Output, Failure: Error> : Subscription {
    let capacity: Int
    var subscriber: AnySubscriber<Output, Failure>? = nil
    var demand: Subscribers.Demand = .none
    var buffer: [Output]
    var completion : Subscribers.Completion<Failure>? = nil

    init<S>(capacity: Int, subscriber: S, replay: [Output], completion: Subscribers.Completion<Failure>?) where S: Subscriber, S.Input == Output, Failure == S.Failure {
        self.capacity = capacity
        self.subscriber = AnySubscriber(subscriber)
        self.completion = completion
        self.buffer = replay
    }

    func complete(withCompletion completion: Subscribers.Completion<Failure>) {
        guard let subscriber = subscriber else { return }
        self.subscriber = nil
        self.completion = nil
        self.buffer.removeAll()

        subscriber.receive(completion: completion)
    }

    func emitAsNeeded() {
        guard let subscriber = subscriber else { return }

        while demand > .none, !buffer.isEmpty {
            demand -= .max(1)
            let nextDemand = subscriber.receive(buffer.removeFirst())
            if nextDemand != .none {
                demand += nextDemand
            }
        }

        if let completion = completion {
            complete(withCompletion: completion)
        }
    }

    func request(_ demand: Subscribers.Demand) {
        if demand != .none {
            self.demand = demand
        }
        emitAsNeeded()
    }

    func cancel() {
        complete(withCompletion: .finished)
    }

    func receive(_ input: Output) {
        guard subscriber != nil else {
            return
        }
        buffer.append(input)
        if buffer.count > capacity { buffer.removeFirst() }
        emitAsNeeded()
    }

    func receive(completion: Subscribers.Completion<Failure>) {
        guard let subscriber = subscriber else {
            return
        }
        self.subscriber = nil
        buffer.removeAll()
        subscriber.receive(completion: completion)
    }
}

extension Publishers {
    final class ShareReplay<Upstream: Publisher> : Publisher {

        typealias Output = Upstream.Output
        typealias Failure = Upstream.Failure

        let upsteam: Upstream
        let capacity: Int
        let lock = NSRecursiveLock()
        var replay = [Output]()
        var subscriptions = [ShareReplaySubscription<Output, Failure>]()
        var completion: Subscribers.Completion<Failure>? = nil

        init(upsteam: Upstream, capacity: Int) {
            self.upsteam = upsteam
            self.capacity = capacity
        }

        func relay(_ value: Output) {
            lock.lock()
            defer {
                lock.unlock()
            }

            guard completion == nil else {
                return
            }

            replay.append(value)
            if replay.count > capacity { replay.removeFirst() }

            subscriptions.forEach { $0.receive(value) }
        }

        func complete(_ completion: Subscribers.Completion<Failure>) {
            lock.lock()
            defer {
                lock.unlock()
            }

            self.completion = completion
            subscriptions.forEach { $0.receive(completion: completion) }
        }

        func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
            lock.lock()
            defer {
                lock.unlock()
            }
            let subscription = ShareReplaySubscription(capacity: capacity, subscriber: subscriber, replay: replay, completion: completion)
            subscriptions.append(subscription)
            subscriber.receive(subscription: subscription)

            guard subscriptions.count == 1 else {
                return
            }

            let subscriber = AnySubscriber { (subscription) in
                subscription.request(.unlimited)
            } receiveValue: { [weak self] (value: Output) -> Subscribers.Demand in
                self?.relay(value)
                return .none
            } receiveCompletion: { [weak self] completion in
                self?.complete(completion)
            }
            upsteam.subscribe(subscriber)
        }

    }
}

extension Publisher {
    func shareReplay(capacity: Int = .max) -> Publishers.ShareReplay<Self> {
        return Publishers.ShareReplay(upsteam: self, capacity: capacity)
    }
}

var logger = TimeLogger(sinceOrigin: true)

let subject = PassthroughSubject<Int, Never>()

let publisher = subject.shareReplay(capacity: 2)

subject.send(0)

let subscription1 = publisher.sink { print("Subscription 1 received completion", $0)} receiveValue: {print("Subscription 1 received value", $0) }

subject.send(1)
subject.send(2)
subject.send(3)


let subscription2 = publisher.sink { print("Subscription 2 received completion", $0)} receiveValue: {print("Subscription 2 received value", $0) }

subject.send(4)
subject.send(5)

subject.send(completion: .finished)

var subscription3: AnyCancellable?

DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
    print("Subscribing after completion")
    subscription3 = publisher.sink { print("Subscription 3 received completion", $0)} receiveValue: {print("Subscription 3 received value", $0) }
}

//: [Next](@next)
/*:
 Copyright (c) 2019 Razeware LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 distribute, sublicense, create a derivative work, and/or sell copies of the
 Software in any work that is designed, intended, or marketed for pedagogical or
 instructional purposes related to programming, coding, application development,
 or information technology.  Permission for such use, copying, modification,
 merger, publication, distribution, sublicensing, creation of derivative works,
 or sale is expressly withheld.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
