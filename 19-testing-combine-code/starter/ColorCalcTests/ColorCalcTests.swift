/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACTa, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import XCTest
import Combine
import SwiftUI
@testable import ColorCalc

class ColorCalcTests: XCTestCase {

    var viewModel: CalculatorViewModel!
    var subscriptions = Set<AnyCancellable>()

    func testColorName() {
        let expectedResult = "rwGreen66%"
        var result = ""

        viewModel.$name.sink { result = $0 }
            .store(in: &subscriptions)

        viewModel.hexText = "006636AA"

        XCTAssertEqual(expectedResult, result)
    }

    func testBackspace() {
        let expectedResult = "123"
        var result = ""

        viewModel.hexText = "1234"
        viewModel.$hexText.sink {
            result = $0
            
        }
            .store(in: &subscriptions)

        viewModel.process(CalculatorViewModel.Constant.backspace)

        XCTAssertEqual(expectedResult, result)
    }

    func testBackgroundColorReset() {
        let expectedResult = Color.white
        var result = Color.clear

        viewModel.$color.sink { result = $0 }
            .store(in: &subscriptions)

        viewModel.process(CalculatorViewModel.Constant.backspace)

        XCTAssertEqual(expectedResult, result)

    }

    func testBackgroundColorForInvalidHex() {
        let expectedResult = Color.white
        var result = Color.clear

        viewModel.hexText = "abc"

        viewModel.$color.sink { result = $0 }
            .store(in: &subscriptions)

        viewModel.process(CalculatorViewModel.Constant.backspace)

        XCTAssertEqual(expectedResult, result)

    }

    func testClearTap() {
        let expectedResult = "#"
        var result = "1234"

        viewModel.hexText = "1234"
        viewModel.$hexText.sink { result = $0 }
        .store(in: &subscriptions)

        viewModel.process(CalculatorViewModel.Constant.clear)

        XCTAssertEqual(expectedResult, result)
    }

    func testRgbText() {
        let expectedResult = "0, 102, 54, 255"
        var result = ""

        viewModel.hexText = "006636"
        viewModel.$rgboText.sink{ result = $0 }
            .store(in: &subscriptions)

        XCTAssertEqual(result, expectedResult)
    }

  override func setUp() {
    viewModel = CalculatorViewModel()
  }
  
  override func tearDown() {
    subscriptions = []
  }
}
