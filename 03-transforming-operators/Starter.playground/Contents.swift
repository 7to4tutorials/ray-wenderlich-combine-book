import Foundation
import Combine

var subscriptions = Set<AnyCancellable>()

example(of: "Collect") {
    let publisher = (1...6).publisher

    publisher
        .collect(2)
        .sink(receiveValue: { print("Recieved value ", $0)})

}

example(of: "Map") {
    let publisher = [23, 83, 47].publisher

    let formatter = NumberFormatter()
    formatter.numberStyle = .spellOut

    publisher
        .map{ formatter.string(for: NSNumber(integerLiteral: $0)) ?? "" }
        .sink(receiveValue: { print("Recieved value ", $0) })
}

example(of: "Map KeyPaths") {
    let publisher = PassthroughSubject<Coordinate, Never>()

    publisher
        .map(\.x, \.y)
        .sink { print("The quadrant of ", $0, ",", $1, " is ", quadrantOf(x: $0, y: $1) )
    }

    publisher.send(Coordinate(x: 10, y: -8))
    publisher.send(Coordinate(x: 0, y: 5))
}

example(of: "Try Map") {
    Just("Invalid file path")
        .tryMap { try FileManager.default.contentsOfDirectory(atPath:$0) }
        .sink { print("Recieved value ", $0)}
            receiveValue: { print("Recieved value ", $0)}
//        .store(in: &subscriptions)
}

example(of: "Flat Map") {
    let periyarChat = Chatter(name: "Periyar", message: "Maanamum Arivum..")
    let ambedkarChat = Chatter(name: "Ambedkar", message: "Jai Bhim")

    let chatSubject = CurrentValueSubject<Chatter, Never>(periyarChat)

    chatSubject
        .flatMap(maxPublishers: .max(2)){ $0.message }
        .sink(receiveValue: {print($0)})

    periyarChat.message.value = "Yaar sonnaalum..."
    chatSubject.value = ambedkarChat

    periyarChat.message.value = "Punpaduthitaan!"
    ambedkarChat.message.value = "Liberty, Equality, Fraternity"

    let manuChat = Chatter(name: "Manu", message: "Not everyone's equal")

    chatSubject.value = manuChat

    periyarChat.message.value = "Enga area ulla varaatha!"
}

example(of: "Map vs Flat Map") {
    let periyarChat = Chatter(name: "Periyar", message: "Maanamum Arivum..")
    let ambedkarChat = Chatter(name: "Ambedkar", message: "Jai Bhim")

    let chatSubject = CurrentValueSubject<Chatter, Never>(periyarChat)

    chatSubject
        .map{ $0.message }
        .sink(receiveValue: {print($0.value)})

    periyarChat.message.value = "Yaar sonnaalum..."
    chatSubject.value = ambedkarChat

    periyarChat.message.value = "Punpaduthitaan!"
    ambedkarChat.message.value = "Liberty, Equality, Fraternity"
}

example(of: "Replace Nil") {
    ["A", nil, "C"].publisher
        .replaceNil(with: "-")
        .map { $0! }
        .sink { print("Recieved value ", $0)}
}

/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.
