import UIKit
import Combine

var subscriptions = Set<AnyCancellable>()

example(of: "prepend") {
    (3 ... 5).publisher
        .prepend(1,2)
        .prepend(-1,0)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

}

example(of: "sequnce") {
    (3 ... 5).publisher
        .prepend(1...2)
        .prepend(Set(-1...0))
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

}

example(of: "PrependPublisher") {
    let prependPublisher = [1,2].publisher
    let publisher = [3,4].publisher

    publisher
        .prepend(prependPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of: "waitForPrependCompletion") {
    let prependPublisher = PassthroughSubject<Int, Never>()

    [3,4]
        .publisher
        .prepend(prependPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    prependPublisher.send(1)
    prependPublisher.send(2)
    prependPublisher.send(completion: .finished)
}

example(of: "append") {
    [1,2].publisher
        .append(3)
        .append(4,5)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

}

example(of: "waitForAppend") {
    let publisher = PassthroughSubject<Int, Never>()

    publisher
        .append(3)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
    publisher.send(1)
    publisher.send(2)
    publisher.send(completion: .finished)
}

example(of: "append") {
    [1,2].publisher
        .append(3...5)
        .append(stride(from: 6, to: 10, by: 2))
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

}

example(of: "appendPublisher") {
    let publisher = [1,2].publisher
    let appendPublisher = [3,4].publisher

    publisher
        .append(appendPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }
}

example(of: "switchToLatest") {
    let publisher1 = PassthroughSubject<Int, Never>()
    let publisher2 = PassthroughSubject<Int, Never>()
    let publisher3 = PassthroughSubject<Int, Never>()

    let latestPublisher = PassthroughSubject<PassthroughSubject<Int, Never>, Never>()

    latestPublisher
        .switchToLatest()
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    publisher1.send(0)
    latestPublisher.send(publisher1)
    publisher1.send(1)
    publisher1.send(2)

    publisher2.send(3)
    latestPublisher.send(publisher2)
    publisher1.send(10)
    publisher2.send(4)

    latestPublisher.send(publisher3)
    publisher3.send(5)

    latestPublisher.send(completion: .finished)
    publisher3.send(6)
    publisher3.send(completion: .finished)

}

example(of: "switchToLatest vs nothing") {
    let publisher1 = CurrentValueSubject<Int, Never>(-1)
    let publisher2 = CurrentValueSubject<Int, Never>(-1)
    let publisher3 = CurrentValueSubject<Int, Never>(-1)

    let latestPublisher = PassthroughSubject<CurrentValueSubject<Int, Never>, Never>()

    latestPublisher
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0.value) }

    publisher1.send(0)
    latestPublisher.send(publisher1)
    publisher1.send(1)
    publisher1.send(2)

    publisher2.send(3)
    latestPublisher.send(publisher2)
    publisher1.send(10)
    publisher2.send(4)

    latestPublisher.send(publisher3)
    publisher3.send(5)

    latestPublisher.send(completion: .finished)
    publisher3.send(6)
    publisher3.send(completion: .finished)

}


example(of: "switchToLatest vs flatMap") {
    let publisher1 = CurrentValueSubject<Int, Never>(-1)
    let publisher2 = CurrentValueSubject<Int, Never>(-1)
    let publisher3 = CurrentValueSubject<Int, Never>(-1)

    let latestPublisher = CurrentValueSubject<CurrentValueSubject<Int, Never>, Never>(publisher1)

    latestPublisher
        .flatMap { $0 }
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    publisher1.send(0)
    latestPublisher.send(publisher1)
    publisher1.send(1)
    publisher1.send(2)

    publisher2.send(3)
    latestPublisher.send(publisher2)
    publisher1.send(10)
    publisher2.send(4)

    latestPublisher.send(publisher3)
    publisher3.send(5)

    latestPublisher.send(completion: .finished)
    publisher3.send(6)
    publisher3.send(completion: .finished)

}

//example(of: "switchToLatestRequest") {
//    let url = URL(string: "https://source.unsplash.com/random")!
//    func fetchImage() -> AnyPublisher<UIImage?, Never > {
//        return URLSession.shared
//            .dataTaskPublisher(for: url)
//            .map { data, _ in UIImage(data: data) }
//            .print("image")
//            .replaceError(with: nil)
//            .eraseToAnyPublisher()
//    }
//
//    let taps = PassthroughSubject<Void, Never>()
//
//    taps
//        .map{ _ in fetchImage()}
//        .switchToLatest()
//        .sink(receiveValue: { _ in })
//        .store(in: &subscriptions)
//
//    taps.send()
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//        taps.send()
//    }
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3.1) {
//        taps.send()
//    }
//}
//
//
//example(of: "switchToLatestRequest vs no switch") {
//    let url = URL(string: "https://source.unsplash.com/random")!
//    func fetchImage() -> AnyPublisher<UIImage?, Never > {
//        return URLSession.shared
//            .dataTaskPublisher(for: url)
//            .map { data, _ in UIImage(data: data) }
//            .print("image")
//            .replaceError(with: nil)
//            .eraseToAnyPublisher()
//    }
//
//    let taps = PassthroughSubject<Void, Never>()
//
//    taps
//        .map{ _ in fetchImage()}
////        .switchToLatest()
//        .sink(receiveValue: { _ in })
//        .store(in: &subscriptions)
//
//    taps.send()
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//        taps.send()
//    }
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3.1) {
//        taps.send()
//    }
//}
//
//example(of: "switchToLatestRequest vs flatMapRequest") {
//    let url = URL(string: "https://source.unsplash.com/random")!
//    func fetchImage() -> AnyPublisher<UIImage?, Never > {
//        return URLSession.shared
//            .dataTaskPublisher(for: url)
//            .map { data, _ in UIImage(data: data) }
//            .print("image")
//            .replaceError(with: nil)
//            .eraseToAnyPublisher()
//    }
//
//    let taps = PassthroughSubject<Void, Never>()
//
//    taps
//        .map{ _ in fetchImage()}
//        .flatMap{ $0 }
//        .sink(receiveValue: { _ in })
//        .store(in: &subscriptions)
//
//    taps.send()
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//        taps.send()
//    }
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 3.1) {
//        taps.send()
//    }
//}

example(of: "Merge") {
    let publisher1 = PassthroughSubject<Int, Never>()
    let publisher2 = PassthroughSubject<Int, Never>()

    publisher2
        .merge(with: publisher1)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    publisher1.send(0)
    publisher2.send(1)
    publisher1.send(2)
    publisher2.send(3)
    publisher1.send(4)

    publisher1.send(completion: .finished)
    publisher1.send(6)
    publisher2.send(completion: .finished)
}

example(of: "Merge vs FlatMap") {
    let publisher1 = PassthroughSubject<Int, Never>()
    let publisher2 = PassthroughSubject<Int, Never>()

    let publisherOfPublishers = PassthroughSubject<PassthroughSubject<Int, Never>, Never>()

    publisherOfPublishers
        .flatMap{ $0 }
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value ", $0) }

    publisherOfPublishers.send(publisher1)
    publisherOfPublishers.send(publisher2)

    publisher1.send(0)
    publisher2.send(1)
    publisher1.send(2)
    publisher2.send(3)
    publisher1.send(4)

    publisher1.send(completion: .finished)
    publisher1.send(6)
    publisher2.send(completion: .finished)
}

example(of: "combineLatest") {
    let stringPublisher = PassthroughSubject<String, Never>()
    let intPublisher = PassthroughSubject<Int, Never>()

    intPublisher
        .combineLatest(stringPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value: ", $0, $1) }

    intPublisher.send(1)
    intPublisher.send(2)
    stringPublisher.send("a")
    stringPublisher.send("b")

    intPublisher.send(3)

}

example(of: "combineLatestWithSelf") {
    let stringPublisher = PassthroughSubject<String, Never>()
    let intPublisher = PassthroughSubject<Int, Never>()

    intPublisher
        .combineLatest(intPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value: ", $0, $1) }

    intPublisher.send(1)
    intPublisher.send(2)
    stringPublisher.send("a")
    stringPublisher.send("b")

    intPublisher.send(3)

}

example(of: "zip") {
    let stringPublisher = PassthroughSubject<String, Never>()
    let intPublisher = PassthroughSubject<Int, Never>()

    intPublisher
        .zip(stringPublisher)
        .sink { print("Recieved completion ", $0) } receiveValue: { print("Recieved value: ", $0, $1) }

    intPublisher.send(1)
    intPublisher.send(2)

    stringPublisher.send("a")
    stringPublisher.send("b")
    stringPublisher.send("c")
    stringPublisher.send("d")

    intPublisher.send(3)

    stringPublisher.send(completion: .finished)

}
