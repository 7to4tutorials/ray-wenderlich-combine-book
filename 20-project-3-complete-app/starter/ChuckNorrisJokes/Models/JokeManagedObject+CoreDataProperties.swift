//
//  JokeManagedObject+CoreDataProperties.swift
//  ChuckNorrisJokes
//
//  Created by Jagan Moorthy on 2020-11-17.
//  Copyright © 2020 Scott Gardner. All rights reserved.
//
//

import Foundation
import CoreData


extension JokeManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<JokeManagedObject> {
        return NSFetchRequest<JokeManagedObject>(entityName: "JokeManagedObject")
    }

    @NSManaged public var categories: NSObject?
    @NSManaged public var id: String?
    @NSManaged public var languageCode: String?
    @NSManaged public var translatedValue: String?
    @NSManaged public var translationLanguageCode: String?
    @NSManaged public var value: String?

}

extension JokeManagedObject : Identifiable {

}
