//
//  JokeManagedObjectExtension.swift
//  ChuckNorrisJokes
//
//  Created by Jagan Moorthy on 2020-11-17.
//  Copyright © 2020 Scott Gardner. All rights reserved.
//

import ChuckNorrisJokesModel
import Combine
import CoreData
import Foundation
import SwiftUI

extension JokeManagedObject {

    static func save(joke: Joke, inContext context: NSManagedObjectContext) {
//        guard joke.id != "error" else { return }

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: self))
        fetchRequest.predicate = NSPredicate(format: "id = %@", joke.id)

        if let results = try? context.fetch(fetchRequest),
           let existingJoke = results.first as? JokeManagedObject {
            existingJoke.categories = joke.categories as NSArray
            existingJoke.languageCode = joke.languageCode
            existingJoke.translatedValue = joke.translatedValue
            existingJoke.translationLanguageCode = joke.translationLanguageCode
            existingJoke.value = joke.value
        } else {
            let jokeManagedObject = JokeManagedObject(context: context)
            jokeManagedObject.categories = joke.categories as NSArray
            jokeManagedObject.languageCode = joke.languageCode
            jokeManagedObject.translatedValue = joke.translatedValue
            jokeManagedObject.translationLanguageCode = joke.translationLanguageCode
            jokeManagedObject.value = joke.value
            jokeManagedObject.id = joke.id
        }

        do {
            try context.save()
        } catch {

        }
    }
}

extension Collection where Element == JokeManagedObject, Index == Int {
    func delete(at indices: IndexSet, context: NSManagedObjectContext) {
        indices.forEach { (index) in
            context.delete(self[index])
        }
        do {
            try context.save()
        } catch {

        }
    }
}
