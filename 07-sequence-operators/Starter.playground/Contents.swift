import Foundation
import Combine

var subscriptions = Set<AnyCancellable>()

example(of: "min") {
    [1, 25, -43, 0, 2].publisher
        .print("Publisher")
        .min()
        .sink { print("Received value", $0) }
}

example(of: "minBy") {
    ["12345", "jfsldjflkas", "sadfaesd"].publisher
        .compactMap{ $0.data(using: .utf8) }
        .print("Publisher")
        .min(by: { $0.count < $1.count })
        .sink { print("Received value: ", String(data: $0, encoding: .utf8)) }
}

example(of: "max") {
    [1, 25, -43, 0, 2].publisher
        .print("Publisher")
        .max()
        .sink { print("Received value", $0) }
}

example(of: "first") {
    let publisher = (1...5).publisher.print("original publisher")

    let upstreamSubscriber = publisher
        .print("upstreamSubscriber publisher")
        .sink { print("upstreamSubscriber Received value", $0) }

    let firstSubscriber = publisher
        .print("firstSubscriber publisher")
        .first()
        .sink { print("firstSubscriber Received value", $0) }

}

example(of: "firstAfterCancellation") {
    let subject = PassthroughSubject<Int, Never>()
    subject.print("original publisher")

    let upstreamSubscriber = subject
        .print("upstreamSubscriber publisher")
        .sink { print("upstreamSubscriber Received value", $0) }

    let firstSubscriber = subject
        .print("firstSubscriber publisher")
        .first()
        .sink { print("firstSubscriber Received value", $0) }

    subject.send(1)
    subject.send(2)
    subject.send(3)

}

example(of: "firstWhere") {
    (1...10).publisher
        .print("publsiher")
        .first(where: { $0 > 5 })
        .sink { print("firstSubscriber Received value", $0) }
}

example(of: "last") {
    let publisher = (1...5).publisher
//        .print("original publisher")

//    let upstreamSubscriber = publisher
//        .print("upstreamSubscriber publisher")
//        .sink { print("upstreamSubscriber Received value", $0) }

    let lastSubscriber = publisher
        .print("lastSubscriber publisher")
        .last()
        .sink { print("lastSubscriber Received value", $0) }

}

example(of: "outputAt") {
    (1...5).publisher
        .print("publisher")
        .output(at: 2)
        .sink { print("Received value", $0) }
}

example(of: "outputIn") {
    (1...10).publisher
        .print("Publisher")
        .output(in: 3...7)
        .sink { print("Received value", $0) }
}

example(of: "count") {
    (1...5).publisher
        .print("publisher")
        .count()
        .sink { print("Received value", $0) }
}

example(of: "contains") {
    (1...5).publisher
        .print("publisher")
        .contains(6)
        .sink { print("Received value", $0) }
}

example(of: "ConstainsWhere") {
    struct Person {
        let id: Int
        let name: String
    }

    [(1, "Periyar"), (2, "Ambedkar")].map(Person.init)
        .publisher
        .print("Publisher")
        .contains(where: { $0.id == 3  || $0.name == "Periyar" })
        .sink { print("Received value", $0) }
}

example(of: "allSatisfy") {
    struct Person {
        let id: Int
        let name: String
    }

    [(1, "Periyar"), (2, "Ambedkar"), (420, "Manu")].map(Person.init)
        .publisher
        .print("Publisher")
        .allSatisfy{ $0.id != 420 }
        .sink { print("Are all good people?", $0) }
}

example(of: "reduce") {
    (1...5).publisher
        .print("Publisher")
        .reduce(0) { $0 + $1 }
        .sink { print("Received value", $0) }
}

example(of: "reduceShortSyntax") {
    (1...5).publisher
        .print("Publisher")
        .reduce(0, +)
        .sink { print("Received value", $0) }
}

example(of: "reduceVsScan") {
    let publisher = (1...5).publisher

    publisher
        .reduce(0, +)
        .sink { print("Reduce Received value", $0) }

    publisher
        .scan(0) { $0 + $1 }
        .sink { print("Scan Received value", $0) }
}

/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.
