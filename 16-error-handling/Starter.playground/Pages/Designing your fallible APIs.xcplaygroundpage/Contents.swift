//: [Previous](@previous)
import Foundation
import Combine

var subscriptions = Set<AnyCancellable>()
//: ## Designing your fallible APIs
example(of: "Designing APIs") {
    class DadJokes {
        struct Joke: Codable {
            let id: String
            let joke: String
        }

        enum Error: Swift.Error, CustomStringConvertible {

            case network
            case parsing
            case jokeDoesntExist(id: String)
            case unknown


            var description: String {
                switch self {
                case .network:
                    return "Request failed. Check internet connectivity"
                case .parsing:
                    return "Invalid response"
                case .jokeDoesntExist(id: let id):
                    return "No joke found with ID " + id
                case .unknown:
                    return "Something went wrong!"
                }
            }
        }

        func fetchJoke(withID id: String) -> AnyPublisher<Joke, Error> {
            guard id.rangeOfCharacter(from: .letters) != nil else {
                return Fail<Joke, Error>(error: Error.jokeDoesntExist(id: id))
                    .eraseToAnyPublisher()
            }
            let url = URL(string: "https://icanhazdadjoke.com/j/\(id)")
            var request = URLRequest(url: url ?? URL(string: "https://www.google.com")!)
            request.allHTTPHeaderFields = ["Accept": "application/json"]
            return URLSession.shared.dataTaskPublisher(for: request)
                .print("Data task")
                .map(\.data)
                .decode(type: Joke.self, decoder: JSONDecoder())
                .mapError { (error) -> Error in
                    switch error {
                    case is URLError: return Error.network
                    case is DecodingError: return Error.parsing
                    default: return .unknown
                    }
                }
                .eraseToAnyPublisher()
        }
    }

    let jokesAPI = DadJokes()
    let jokeID = "9prWnjyImyd"
    let badJokeID = "123456"


    jokesAPI.fetchJoke(withID: jokeID)
    .sink { print("Received completion", $0) } receiveValue: { print("Received value", $0) }
    .store(in: &subscriptions)
}

example(of: "Joke API") {
    return
    class DadJokes {
        // 1
        struct Joke: Codable {
            let id: String
            let joke: String
        }
        // 2
        func getJoke(id: String) -> AnyPublisher<Joke, Error> {
            let url = URL(string: "https://icanhazdadjoke.com/j/\(id)")!
            var request = URLRequest(url: url)
            request.allHTTPHeaderFields = ["Accept": "application/json"]
            return URLSession.shared
                .dataTaskPublisher(for: request)
                .map(\.data)
                .print("Data")
                .decode(type: Joke.self, decoder: JSONDecoder())
                .eraseToAnyPublisher()
        }

        func fetchResponse(id: String) -> AnyPublisher<String?, URLError> {
            let url = URL(string: "https://icanhazdadjoke.com/j/\(id)")!
            var request = URLRequest(url: url)
//            request.allHTTPHeaderFields = ["Accept": "application/json"]
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            return URLSession.shared
                .dataTaskPublisher(for: request)
                .map(\.data)
                .map{ String(data: $0, encoding: .utf8) }
//                .print("Data")
//                .decode(type: Joke.self, decoder: JSONDecoder())
                .eraseToAnyPublisher()
        }

        func dataTask() {
            let url = URL(string: "https://icanhazdadjoke.com/j/9prWnjyImyd")!
            var request = URLRequest(url: url)
            //            request.allHTTPHeaderFields = ["Accept": "application/json"]
//            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            URLSession.shared.dataTask(with: request) { (data, res, error) in
                if let data = data {
                    let string = String(data: data, encoding: .utf8)
                } else if let error = error {

                }
            }.resume()
        }

    }

    // 4
    let api = DadJokes()
    let jokeID = "9prWnjyImyd"
    let badJokeID = "123456"
    // 5
//    api
//        .getJoke(id: jokeID) .sink(receiveCompletion: { print($0) },
//                                   receiveValue: { print("Got joke: \($0)") }) .store(in: &subscriptions)

//    api
//        .fetchResponse(id: jokeID) .sink(receiveCompletion: { print($0) },
//                                   receiveValue: { print("Got joke: \($0)") }) .store(in: &subscriptions)

    let harcodedResponse = "{\"id\":\"9prWnjyImyd\",\"joke\":\"Why do bears have hairy coats? Fur protection.\",\"status\":200}".data(using: .utf8)
    let responseJoke = try? JSONDecoder().decode(DadJokes.Joke.self, from: harcodedResponse!)
    api.dataTask()
}


//: [Next](@next)

/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.
