/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Combine
import UIKit

class MainViewController: UIViewController {
  
  // MARK: - Outlets

  @IBOutlet weak var imagePreview: UIImageView! {
    didSet {
      imagePreview.layer.borderColor = UIColor.gray.cgColor
    }
  }
  @IBOutlet weak var buttonClear: UIButton!
  @IBOutlet weak var buttonSave: UIButton!
  @IBOutlet weak var itemAdd: UIBarButtonItem!

  // MARK: - Private properties
    private var images = CurrentValueSubject<[UIImage], Never>([])
    private var subscriptions = Set<AnyCancellable>()
  

  // MARK: - View controller
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let collageSize = imagePreview.frame.size

    images
        .handleEvents(receiveOutput: { [weak self] photos in
            self?.updateUI(photos: photos)
        })
        .map { UIImage.collage(images: $0, size: collageSize) }
        .assign(to: \.image, on: imagePreview)
        .store(in: &subscriptions)

  }
  
  private func updateUI(photos: [UIImage]) {
    buttonSave.isEnabled = photos.count > 0 && photos.count % 2 == 0
    buttonClear.isEnabled = photos.count > 0
    itemAdd.isEnabled = photos.count < 6
    title = photos.count > 0 ? "\(photos.count) photos" : "Collage"
  }
  
  // MARK: - Actions
  
  @IBAction func actionClear() {
    images.send([])
  }
  
  @IBAction func actionSave() {
    guard let image = imagePreview.image else { return }
    PhotoWriter.save(image: image)
        .sink { [weak self] result  in
            switch result {
            case .finished:
                break
            case let .failure(error):
                self?.showMessage("Error", description: error.localizedDescription)
            }
        } receiveValue: { [weak self] in
            self?.showMessage("Saved with ID: \($0)")
        }

    
  }
  
  @IBAction func actionAdd() {
    let photosVC = storyboard?.instantiateViewController(identifier: "PhotosViewController") as! PhotosViewController
    let newPhotos = photosVC.selectedPhotos
        .prefix(while: { [weak self] _ in
            self?.images.value.count ?? 7 < 6
        })
        .filter { [weak self] image in
            guard self != nil else { return false }
            return image.size.height <= image.size.width
        }
        .share()

// TODO: Check what happens on .append instead of .map
    newPhotos
        .map {  [weak self] in
            return (self?.images.value ?? []) + [$0] }
        .assign(to: \.value, on: images)
        .store(in: &subscriptions)

    photosVC.$selectedPhotosCount
        .filter{ $0 > 0 }
        .map { "Selected \($0) photos" }
        .assign(to: \.title, on: self)
        .store(in: &subscriptions)

    newPhotos
        .ignoreOutput()
        .delay(for: 2.0, scheduler: DispatchQueue.main)
        .sink { [weak self] _ in
            guard let self = self else { return }
            self.updateUI(photos: self.images.value)
        } receiveValue: { (_) in }
        .store(in: &subscriptions)

    newPhotos
        .filter {
            [weak self] _ in
                guard let self = self else { return false }
            return self.images.value.count == 6
        }
        .flatMap {  _ in self.showAlert("Limit Reached", description: "To add more than 6 images, purchase pro version") }
        .sink { _ in self.navigationController?.popViewController(animated: true) }
        .store(in: &subscriptions)

    navigationController?.pushViewController(photosVC, animated: false)
  }
  
  private func showMessage(_ title: String, description: String? = nil) {
    _ =  showAlert(title, description: description)
  }
}
