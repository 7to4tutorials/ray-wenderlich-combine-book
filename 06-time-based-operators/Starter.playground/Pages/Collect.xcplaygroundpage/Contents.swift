import Combine
import SwiftUI
import PlaygroundSupport

let emitsPerSecond = 1.0
let groupingDuration = 4
let collectLimit = 2

let sourceSubject = PassthroughSubject<Date, Never>()
let collectionSubjet = sourceSubject
    .collect(.byTime(DispatchQueue.main, .seconds(groupingDuration)))
    .flatMap{ $0.publisher }
let countLimitedCollectionSubject = sourceSubject
    .collect(.byTimeOrCount(DispatchQueue.main, .seconds(groupingDuration), collectLimit))
    .flatMap{ $0.publisher }

let subscription = Timer.publish(every: 1.0 / emitsPerSecond, on: .main, in: .common)
    .autoconnect()
    .subscribe(sourceSubject)

let sourceTimeLine = TimelineView(title: "Source")
let groupedTimeLine = TimelineView(title: "Collected")
let countLimitedGroupTimeLine = TimelineView(title: "Colelcted with limit")

let view = VStack(spacing: 50) {
    sourceTimeLine
    groupedTimeLine
    countLimitedGroupTimeLine
}

sourceSubject.displayEvents(in: sourceTimeLine)
collectionSubjet.displayEvents(in: groupedTimeLine)
countLimitedCollectionSubject.displayEvents(in: countLimitedGroupTimeLine)

PlaygroundPage.current.liveView = UIHostingController(rootView: view)

//: [Next](@next)
/*:
 Copyright (c) 2019 Razeware LLC
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 distribute, sublicense, create a derivative work, and/or sell copies of the
 Software in any work that is designed, intended, or marketed for pedagogical or
 instructional purposes related to programming, coding, application development,
 or information technology.  Permission for such use, copying, modification,
 merger, publication, distribution, sublicensing, creation of derivative works,
 or sale is expressly withheld.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

